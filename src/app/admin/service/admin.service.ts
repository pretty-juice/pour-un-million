import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Question } from '../../interface/question';
import { DocumentReference } from '@angular/fire/firestore/interfaces';

@Injectable()
export class AdminService {
  constructor(private firestore: AngularFirestore) {
  }

  /**
   * Modifie une question
   */
  edit(question: Question): Promise<void> {
    const questionDoc = this.firestore.doc<Question>(`questions/${question.id}`);
    return questionDoc.update({
        questionFr: question.questionFr,
        questionEn: question.questionEn,
        hard      : question.hard,
      } ,
    );
  }

  /**
   * Ajoute une question
   */
  add(questionFr: string, questionEn: string, hard: boolean): Promise<DocumentReference> {
    // Génération de la nouvelle question
    const question: Question = {
      questionFr,
      questionEn,
      hard,
      yes: 0,
      no : 0,
    };
    return this.firestore.collection('questions').add(question);
  }
}
