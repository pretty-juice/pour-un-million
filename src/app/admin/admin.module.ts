import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {
  MatBadgeModule,
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDialogModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatSnackBarModule,
  MatToolbarModule,
} from '@angular/material';
import { AdminPageComponent } from './component/admin-page/admin-page.component';
import { AdminService } from './service/admin.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuestionViewerComponent } from './component/question-viewer/question-viewer.component';
import { ModalQuestionEditorComponent } from './component/modal-question-editor/modal-question-editor.component';

const routes: Routes = [
  {
    path     : '',
    component: AdminPageComponent,
  },
];

@NgModule({
  declarations   : [AdminPageComponent, QuestionViewerComponent, ModalQuestionEditorComponent],
  imports        : [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,

    // Material TODO: fusionner avec les imports de AppModule
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatExpansionModule,
    MatToolbarModule,
    MatBadgeModule,
    MatCheckboxModule,
    MatDialogModule,
    MatChipsModule,
    MatSnackBarModule,
    MatIconModule,
  ],
  entryComponents: [ModalQuestionEditorComponent],
  providers      : [
    AdminService,
  ],

})
export class AdminModule {
}
