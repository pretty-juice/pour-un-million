import { Component, OnInit } from '@angular/core';
import { QuestionService } from '../../../service/question.service';
import { Question } from '../../../interface/question';
import { take } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { ModalQuestionEditorComponent } from '../modal-question-editor/modal-question-editor.component';

@Component({
  selector   : 'app-admin-question-viewer',
  templateUrl: './question-viewer.component.html',
  styleUrls  : ['./question-viewer.component.scss'],
})
export class QuestionViewerComponent implements OnInit {
  /** Liste des questions */
  questions: Question[];
  questionsFiltered: Question[];

  /** Liste des filtres */
  filters: IFilter = {
    translated: true,
    showHard  : true,
    search    : '',
  };

  constructor(private questionService: QuestionService,
              private dialogService: MatDialog) {
  }

  ngOnInit() {
    this.getQuestionList();
  }

  /**
   * Retourne le compteur pour un OUI ou NO
   */
  getPercentOf(question: Question, yes: boolean): string {
    const total = question.yes + question.no;
    return total > 0 ? `${Math.round(((yes ? question.yes : question.no) * 100) / total)}%` : '';
  }

  /**
   * Ouvre la modal d'édition d'une question
   */
  openEditModal(isNew: boolean, question?: Question): void {
    this.dialogService.open(
      ModalQuestionEditorComponent,
      {
        width: '800px', data: {
          question,
          isNew,
        },
      },
    ).afterClosed().subscribe(() => {
      if (isNew) {
        this.getQuestionList();
      }
    });
  }

  /**
   * Filtre la liste
   */
  filterQuestions(): void {
    this.questionsFiltered = this.questions.filter(question => {

      // Retire les questions non traduites dans les deux langues
      if (!this.filters.translated &&
        (((!!question.questionEn && question.questionEn.length > 0)) && (!!question.questionEn && question.questionEn.length > 0))) {
        return false;
      }

      // Retire les questions hard
      if (!this.filters.showHard && question.hard) {
        return false;
      }

      // Retourne uniquement les questions comprenant la recherche (FR ou EN)
      if (this.filters.search && this.filters.search.length > 0) {
        const str = (question.questionFr + question.questionEn).toLocaleLowerCase().replace(/\s/g, '');
        const search = this.filters.search.replace(/\s/g, '').toLocaleLowerCase();
        return str.includes(search);
      }
      return true;
    });
  }

  /**
   * Récupère la liste des questions
   */
  private getQuestionList(): void {
    this.questionService.getQuestions()
      .pipe(
        take(1),
      )
      .subscribe((questions) => {
        this.questions = questions;
        this.filterQuestions();
      });
  }
}

interface IFilter {
  translated: boolean;
  showHard: boolean;
  search: string;
}
