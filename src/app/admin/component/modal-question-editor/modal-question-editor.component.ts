import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { Question } from '../../../interface/question';
import { AdminService } from '../../service/admin.service';

@Component({
  templateUrl: './modal-question-editor.component.html',
  styleUrls  : ['./modal-question-editor.component.scss'],
})
export class ModalQuestionEditorComponent implements OnInit {
  form: FormGroup;
  loading: boolean;

  constructor(@Inject(MAT_DIALOG_DATA) public data: ModalQuestionEditorData,
              public modalRef: MatDialogRef<ModalQuestionEditorComponent>,
              private adminService: AdminService,
              private snackBar: MatSnackBar,
              private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    const question = this.data.question;
    this.form = this.formBuilder.group({
      questionFr: question ? question.questionFr : '',
      questionEn: question ? question.questionEn : '',
      hard      : question ? question.hard : false,
    });
  }

  /**
   * Valide le formulaire
   */
  validate(formValue: FormValue): void {
    if (this.loading) {
      return;
    }
    this.loading = true;
    this.data.isNew ? this.add(formValue) : this.edit(formValue);
  }

  /**
   * Ajoute une nouvelle question
   */
  private add(formValue: FormValue): void {
    this.adminService.add(formValue.questionFr, formValue.questionEn, formValue.hard).then(() => {
      this.snackBar.open('Question ajoutée avec succès !', '', { duration: 3000 });
      this.modalRef.close();
    }).catch(error => {
      this.loading = false;
      this.snackBar.open('Une erreur est survenue...', '', { duration: 3000 });
    });
  }

  /**
   * Edit une question
   */
  private edit(formValue: FormValue) {
    // On met à jour les données de la question
    this.data.question.questionFr = formValue.questionFr;
    this.data.question.questionEn = formValue.questionEn;
    this.data.question.hard = formValue.hard;

    // On modifie la question sur Firebase
    this.adminService.edit(this.data.question).then(() => {
      this.snackBar.open('Question éditée avec succès !', '', { duration: 3000 });
      this.modalRef.close();
    }).catch(error => {
      this.loading = false;
      this.snackBar.open('Une erreur est survenue...', '', { duration: 3000 });
    });
  }
}

export interface ModalQuestionEditorData {
  question: Question;
  isNew: boolean;
}

interface FormValue {
  questionFr: string
  questionEn: string;
  hard: boolean;
}
