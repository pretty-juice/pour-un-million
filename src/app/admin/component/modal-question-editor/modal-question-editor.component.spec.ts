import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ModalQuestionEditorComponent } from './modal-question-editor.component';


describe('QuestionEditorComponent', () => {
  let component: ModalQuestionEditorComponent;
  let fixture: ComponentFixture<ModalQuestionEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalQuestionEditorComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalQuestionEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
