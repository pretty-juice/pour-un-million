import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../service/authentication.service';
import { Router } from '@angular/router';

@Component({
  templateUrl: './admin-page.component.html',
  styleUrls  : ['./admin-page.component.scss'],
})
export class AdminPageComponent {
  constructor(private authenticationService: AuthenticationService,
              private router: Router) {
  }

  /**
   * Déconnecte l'utilisateur
   */
  logOut(): void {
    this.authenticationService.logOut().then(() => {
      this.router.navigate(['/auth']);
    });
  }
}
