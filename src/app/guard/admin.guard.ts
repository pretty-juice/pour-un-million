import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, UrlSegment } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../service/authentication.service';
import { map, take, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AdminGuard implements CanLoad {
  constructor(private authenticationService: AuthenticationService, private router: Router) {
  }

  /**
   * Vérifie si l'utilisateur est connecté pour accéder à la page d'admin et de load le module
   */
  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> {
    return this.authenticationService.user.pipe(
      take(1),
      map(user => !!user),
      tap(loggedIn => {
        if (!loggedIn) {
          this.router.navigate(['/auth']).then(() => {
            console.error('access denied.');
          });
        } else {
          console.log('access granted.');
        }
      }),
    );
  }
}
