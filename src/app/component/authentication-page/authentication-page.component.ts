import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../service/authentication.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector   : 'app-authentication-page',
  templateUrl: './authentication-page.component.html',
  styleUrls  : ['./authentication-page.component.scss'],
})
export class AuthenticationPageComponent implements OnInit {
  form: FormGroup;
  errorMessage: string;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthenticationService,
    private router: Router) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      login   : '',
      password: '',
    });
  }

  /**
   * Connecte l'utilisateur
   */
  logIn(formValue: IFormData): void {
    this.authService.logIn(formValue.login, formValue.password).then(() => {
      this.router.navigate(['/admin']);
    }, error => {
      this.errorMessage = error.message;
    });
  }
}

interface IFormData {
  login: string;
  password: string;
}
