import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { QuestionService } from '../../service/question.service';
import { Question } from '../../interface/question';
import { take } from 'rxjs/operators';
import { Array } from '../../tool/array';
import { animate, AnimationBuilder, keyframes, style } from '@angular/animations';

@Component({
  selector   : 'app-questioner',
  templateUrl: './questioner.component.html',
  styleUrls  : ['./questioner.component.scss'],
})
export class QuestionerComponent implements OnInit {
  /** Liste des questions */
  questions: Question[];

  /** Si les question hard sont affichées */
  showHard = false;

  /** Si le résultat de la question courrante est affiché */
  showResult = false;

  /** La référence des containers de question et réponse */
  @ViewChild('questionContainer', { static: false }) questionContainerRef: ElementRef;

  /** Liste des questions stockées sur Firestore */
  private questionsStored: Question[];

  constructor(
    private questionService: QuestionService,
    private animationBuilder: AnimationBuilder,
  ) {
  }

  /**
   * Récupère la question courrante
   */
  get currentQuestion(): Question {
    return this.questions[0];
  }

  ngOnInit() {
    // Récupération de la liste des questions
    this.questionService.getQuestions()
      .pipe(take(1))
      .subscribe(questions => {
        this.questionsStored = Array.shuffle(questions);
        this.generateQuestionList(this.showHard);
      });
  }

  /**
   * Génère une liste de question à afficher à l'utilisateur en fonction
   * de si il y a déjà répondu et si il change la visibilité des questions hard
   */
  generateQuestionList(showHard: boolean) {
    this.questions = this.questionsStored.slice().filter(question => {
      if (question.hasAnswered) {
        return false;
      }
      if (!showHard) {
        return !question.hard;
      }
      return question;
    });
  }

  /**
   * Change la valeur de showHard et met à jour la liste des questions
   */
  changeShowHard(showHard: boolean) {
    this.generateQuestionList(showHard);
    this.showHard = showHard;
  }

  /**
   * Répond à une question par oui pour par non
   */
  answer(question: Question, result: boolean): void {
    if (question.hasAnswered) {
      return;
    }

    // On met à jour la question sur Firestore
    this.questionService.answer(question, result);
    question.hasAnswered = true;

    // On simule l'incrémentation du compteur
    result ? question.yes++ : question.no++;

    // On affiche le résultat
    this.showResult = true;
  }

  /**
   * Passe à la question suivante
   */
  nextQuestion(): void {
    this.showResult = false;

    // On anime le changement de question
    const leaveAnimation = this.animationBuilder.build([
      style({ top: 0, opacity: 1 }),
      animate(350, style({ top: '20px', opacity: 0 })),
    ]);

    const enterAnimation = this.animationBuilder.build([
      style({ top: '-20px', opacity: 0 }),
      animate(350, style({ top: 0, opacity: 1 })),
    ]);

    const playerLeave = leaveAnimation.create(this.questionContainerRef.nativeElement);
    playerLeave.play();
    playerLeave.onDone(() => {
      // Une fois l'animation de leave terminé, on affiche la nouvelle question puis on anime enter
      this.questions.shift();

      const playerEnter = enterAnimation.create(this.questionContainerRef.nativeElement);
      playerEnter.play();
    });
  }

  /**
   * Retourne le pourcentage de oui ou non d'une question
   */
  getPercentFor(question: Question, yes: boolean): number {
    const value = yes ? question.yes : question.no;
    return Math.floor((value * 100) / (question.yes + question.no));
  }

  /**
   * Retourne si il reste des questions restantes
   */
  areRemainingQuestions(): boolean {
    return this.questions.length > 0;
  }
}
