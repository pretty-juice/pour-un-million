export interface Question {
  id?: string;
  questionFr: string;
  questionEn: string;
  hard: boolean;
  yes: number;
  no: number;
  hasAnswered?: boolean;
}

