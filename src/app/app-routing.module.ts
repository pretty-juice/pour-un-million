import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainPageComponent } from './component/main-page/main-page.component';
import { NotFoundComponent } from './component/not-found/not-found.component';
import { AuthenticationPageComponent } from './component/authentication-page/authentication-page.component';
import { AdminGuard } from './guard/admin.guard';


const routes: Routes = [
  // Page principale
  {
    path     : '',
    component: MainPageComponent,
  },

  // Page d'authentification
  {
    path     : 'auth',
    component: AuthenticationPageComponent,
  },

  // Page admin
  {
    path        : 'admin',
    loadChildren: './admin/admin.module#AdminModule',
    canLoad     : [AdminGuard],
  },

  // 404
  {
    path     : '**',
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
