import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'firebase';
import { Observable } from 'rxjs';
import UserCredential = firebase.auth.UserCredential;

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  /** L'utilisateur connecté */
  user: Observable<User | null>;

  constructor(private fireAuth: AngularFireAuth) {
    this.user = this.fireAuth.authState;
  }

  /**
   * Authentifie l'utilisateur
   */
  logIn(email: string, password: string): Promise<UserCredential> {
    return this.fireAuth.auth.signInWithEmailAndPassword(email, password);
  }

  /**
   * Déconnecte l'utilisateur
   */
  logOut(): Promise<void> {
    return this.fireAuth.auth.signOut();
  }
}
