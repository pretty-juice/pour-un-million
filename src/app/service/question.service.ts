import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Question } from '../interface/question';
import * as firebase from 'firebase/app'; // <--- FIXME cet import

@Injectable({
  providedIn: 'root',
})
export class QuestionService {
  private questionsCollection: AngularFirestoreCollection<Question>;
  private readonly questions: Observable<Question[]>;

  constructor(private firestore: AngularFirestore) {
    this.questionsCollection = firestore.collection('questions');
    this.questions = this.questionsCollection.snapshotChanges().pipe(
      map(changes => {
        return changes.map(a => {
          const question = a.payload.doc.data() as Question;
          question.id = a.payload.doc.id;
          question.hasAnswered = false;
          return question;
        });
      }),
    );
  }

  /**
   * Retourne la liste des questions
   */
  getQuestions(): Observable<Question[]> {
    return this.questions;
  }

  /**
   * Répond à une question
   */
  answer(question: Question, result: boolean) {
    const questionDocRef = this.firestore.doc<Question>(`questions/${question.id}`);
    const incrementator = firebase.firestore.FieldValue.increment(1);
    const fieldToUpdate = result ? { yes: incrementator } : { no: incrementator };

    // @ts-ignore
    questionDocRef.update(fieldToUpdate);
  }
}
