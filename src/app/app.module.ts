import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { QuestionerComponent } from './component/questioner/questioner.component';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { MainPageComponent } from './component/main-page/main-page.component';
import { NotFoundComponent } from './component/not-found/not-found.component';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AuthenticationPageComponent } from './component/authentication-page/authentication-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCardModule, MatFormFieldModule, MatInputModule, MatToolbarModule } from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    QuestionerComponent,
    MainPageComponent,
    NotFoundComponent,
    AuthenticationPageComponent,
  ],
  imports     : [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    // Firebase
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    BrowserAnimationsModule,

    // Material
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatToolbarModule,
  ],
  providers   : [],
  bootstrap   : [AppComponent],
})
export class AppModule {
}
