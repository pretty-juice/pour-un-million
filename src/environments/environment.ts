// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase  : {
    apiKey           : 'AIzaSyDb-nK9SBhIA028_m979ESQEe6Z4Db1wgo',
    authDomain       : 'pour-un-million-ccf96.firebaseapp.com',
    databaseURL      : 'https://pour-un-million-ccf96.firebaseio.com',
    projectId        : 'pour-un-million-ccf96',
    storageBucket    : 'pour-un-million-ccf96.appspot.com',
    messagingSenderId: '1041821817687',
    appId            : '1:1041821817687:web:58435580c7dd85facfbc12',
    measurementId    : 'G-7S222CM5K5',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
